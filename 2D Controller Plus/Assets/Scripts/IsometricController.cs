﻿using UnityEngine;

public class IsometricController : MonoBehaviour {

    public Rigidbody2D body;

    public float curr_velocity;
    public float accell;
    public float min_velocity;
    public float max_velocity;

    public Collider2D collider;
    
    void Start() {
        body = GetComponent<Rigidbody2D>();
        curr_velocity = 0;
    }
    
    void Update() {
        Input_Check();
    }

    public void Input_Check() {
        float horizontal = Input.GetAxis("Horizontal");

        if (horizontal > 0)
        {
            //move(0, accell, min_velocity,max_velocity);
            move(0, Mathf.Abs (horizontal)*5, null);
        }
        else if (horizontal < 0)
        {
            //move(180, accell, min_velocity, max_velocity);
            move(180, Mathf.Abs(horizontal) * 5, null);
        }
    
        if (Input.GetButtonDown ("Jump")) {
            print("kek");
            move(90, 200,false);
        }

    }

    public void move(float angle, float distance, string[] block_layers)
    {
        distance = distance * Time.deltaTime;
        Vector3 dir = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;

        Vector2 new_pos = new Vector2(transform.position.x + dir.x * distance, transform.position.y + dir.y * distance);

        if (Physics2D.BoxCast(new_pos, collider.bounds.size, 0, new_pos,0,LayerMask.GetMask("BOX")))
        {
            
            print("!");
        }
        else {
            transform.position = new_pos;
        }
    }


    public void move (float angle,float accell,float min_velocity,float max_velocity)
    {
        curr_velocity += accell;

        if (curr_velocity < min_velocity) curr_velocity = min_velocity;
        if (curr_velocity > max_velocity) curr_velocity = max_velocity;

        Vector2 dir = (Quaternion.Euler(0, 0, angle) * Vector2.right);
        body.velocity = new Vector2((dir.x * curr_velocity) * Time.deltaTime, dir.y);
    }

    public void move(float angle,float force,bool delta)
    {
        Vector3 dir = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;

        if (delta)
        {
            body.AddForce((dir * force) * Time.deltaTime);
        }
        else
        {
            body.AddForce((dir * force));

        }
    }
    
    public void stop_moving() {
        curr_velocity = curr_velocity/2;
        body.velocity = new Vector2(body.velocity.x / 2, body.velocity.y);
    }

    public void angle_check(float angle) {
        float range = 90;

        if (angle.inRange(0, range))
        {
            print("Angle 0");
        }
        else if (angle.inRange(90, range)) {
            print("Angle 90");
        }
        else if (angle.inRange(180, range))
        {
            print("Angle 180");
        }
        else if (angle.inRange(270, range))
        {
            print("Angle 270");
        }
    }
    

}
