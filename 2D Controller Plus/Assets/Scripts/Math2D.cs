﻿
public static class Math2D {
    
    public static bool inRange(this float num, float numberToCheck, float range)
    {
        return (num >= numberToCheck - range && num <= numberToCheck + range);
    }
}
